<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Measurement extends Model
{
    const UPDATED_AT = null;

    protected $hidden = ['id', 'category_id', 'created_at'];

    protected $appends = ['date'];

    public function getDateAttribute()
    {
        return $this->attributes['created_at'];
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }
}
