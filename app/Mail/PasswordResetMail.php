<?php

namespace App\Mail;

use \App\PasswordReset;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PasswordResetMail extends Mailable
{
    use Queueable, SerializesModels;

    public function __construct(PasswordReset $passwordReset)
    {
        $this->passwordReset = $passwordReset;
    }

    public function build()
    {
        $link = route('password_reset', ['token' => $this->passwordReset->token]);
        return $this->view('emails.forgot_password')->with(compact('link'))->subject(config('app.name', 'Weather Dashboard') . ' - Password reset');
    }
}
