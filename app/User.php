<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Role;
use Illuminate\Support\Facades\Validator;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function validate_password()
    {
        return [
            'requirements' => [
                'password' => 'required|min:8',
                'confirm-password' => 'required|same:password'
            ],
            'messages' => [
                'required' => 'The :attribute field is required',
                'password.min' => 'Password is less than :min characters',
                'confirm-password.same' => 'Passwords don\'t match'
            ]
        ];
    }

    public static function validate_email()
    {
        return [
            'requirements' => [
                'email' => 'bail|required|email|unique:users'
            ],
            'messages' => [
                'required' => 'The :attribute field is required',
                'email.unique' => 'Email already taken',
                'email.email' => 'Invalid email'
            ]
        ];
    }

    public function role()
    {
        return $this->belongsTo('App\Role');
    }

    public function categories()
    {
        return $this->hasMany('App\Category');
    }

    public function sensors()
    {
        return $this->hasMany('App\Sensor');
    }

    public function password_resets()
    {
        return $this->hasMany('App\PasswordReset');
    }

    public function settings()
    {
        return $this->hasMany('App\Setting');
    }
}
