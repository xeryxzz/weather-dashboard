<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sensor extends Model
{
    const UPDATED_AT = null;

    public function url_name()
    {
        $non_allowed_characters = '/[^0-9A-Za-z\-]+/';
        return rawurlencode(trim(preg_replace($non_allowed_characters, '-', strtolower($this->name)), '-'));
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function categories()
    {
        return $this->hasMany('App\Category');
    }
}
