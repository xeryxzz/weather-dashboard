<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function url_name()
    {
        $non_allowed_characters = '/[^0-9A-Za-z\-]+/';
        return rawurlencode(trim(preg_replace($non_allowed_characters, '-', strtolower($this->name)), '-'));
    }

    public static function validate_color()
    {
        return [
            'requirements' => [
                'color' => 'required|size:6|regex:/^[0-9A-Fa-f]+$/'
            ]
        ];
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function sensor()
    {
        return $this->belongsTo('App\Sensor');
    }

    public function measurements()
    {
        return $this->hasMany('App\Measurement');
    }
}
