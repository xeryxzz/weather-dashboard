<?php

namespace App\Http\Middleware;

use Closure;
use App\Setting;
use Illuminate\Support\Facades\Auth;

class AllowedToInvite
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->role->privilege_level >= intval(Setting::where('key', 'privilege.invite')->first()->value)) {
            return $next($request);
        }
        return redirect()->route('dashboard');
    }
}
