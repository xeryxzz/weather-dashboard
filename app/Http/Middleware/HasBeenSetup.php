<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class HasBeenSetup
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->categories->isNotEmpty()) {
            return $next($request);
        }

        return redirect()->route('connect')->with('message-warning', ['title' => 'Sensor not connected', 'message' => 'Dashboard is not yet connected with any sensor']);
    }
}
