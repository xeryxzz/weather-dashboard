<?php

namespace App\Http\Middleware;

use Closure;
use App\Invite;
use App\Setting;

class RegistrationEnabled
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->route('token');
        $invite_valid = false;
        if ($token != '') {
            $invite = Invite::where('token', $token)->first();
            $invite_valid = !is_null($invite) && !$invite->used;
        }
        if (Setting::where('key', 'registration.enabled')->first()->value === 'true' || $invite_valid) {
            return $next($request);
        }
        return redirect()->route('login')->with('message-warning', ['title' => 'Registration closed', 'message' => 'Registration is currently closed']);
    }
}
