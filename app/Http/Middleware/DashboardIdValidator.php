<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class DashboardIdValidator
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $request->sensors = Auth::user()->sensors;
        $request->categories = Auth::user()->categories;

        $sensor_id = $request->route('sensor_id');
        $category_id = $request->route('category_id');

        $request->current_sensor = null;
        $request->current_category = null;

        if ($sensor_id) {
            $sensor_validator = Validator::make(['sensor_id' => $sensor_id], [
                'sensor_id' => ['integer', Rule::exists('sensors', 'id')->where('user_id', Auth::id())]
            ]);

            if ($sensor_validator->fails()) {
                if ($request->ajax()) {
                    return $sensor_validator->errors()->toJson();
                }
                return redirect()->route('dashboard')->withErrors($sensor_validator->errors());
            }

            $request->current_sensor = $request->sensors->where('id', $sensor_id)->first();
            if ($category_id) {
                $category_validator = Validator::make(['category_id' => $category_id], [
                    'category_id' => ['integer', Rule::exists('categories', 'id')->where('user_id', Auth::id())->where('sensor_id', $sensor_id)]
                ]);

                if ($category_validator->fails()) {
                    if ($request->ajax()) {
                        return $category_validator->errors()->toJson();
                    }
                    return redirect()->route('dashboard')->withErrors($category_validator->errors());
                }

                $request->current_category = $request->categories->where('id', $category_id)->first();
            }
            if (!$request->current_category) {
                foreach ($request->categories as $category) {
                    if ($category->sensor_id == $request->current_sensor->id) {
                        $request->current_category = $category;
                        break;
                    }
                }
            }
        }
        if (!$request->current_sensor) {
            $request->current_sensor = $request->sensors[0];
        }
        if (!$request->current_category) {
            $request->current_category = $request->categories[0];
        }

        return $next($request);
    }
}
