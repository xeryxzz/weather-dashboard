<?php

namespace App\Http\Middleware;

use Closure;
use App\Sensor;
use Illuminate\Support\Facades\Validator;

class ApiAuthentication
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $validator = Validator::make(['token' => $request->bearerToken()], [
           'token' => 'required|exists:sensors'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 'Error', 'messages' => $validator->errors()], 401);
        }

        $sensor = Sensor::where('token', $request->bearerToken())->first();
        if (!is_null($sensor)) {
            $request->sensor = $sensor;
            return $next($request);
        }

    }
}
