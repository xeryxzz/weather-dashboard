<?php

namespace App\Http\Controllers;

use App\Invite;
use Illuminate\Http\Request;

class InviteController extends Controller
{
    public function show_invite_form()
    {
        $invites = Invite::all();

        return view('invite')->with(compact('invites'));
    }

    public function create(Request $request)
    {
        $invite = new Invite;

        $invite->token = null;
        while (is_null($invite->token) || mb_strlen($invite->token) !== 48) {
            try {
                $invite->token = bin2hex(random_bytes(24));
            }
            catch (Exception $e) {
                $invite->token = null;
            }
        }
        $invite->save();

        if ($request->ajax()) {
            return [
                'id' => $invite->id,
                'url' => route('register', ['token' => $invite->token])
            ];
        }

        return redirect()->route('invite')->with('message-success', ['title' => 'Created invite', 'message' => 'Successfully created invite']);
    }

    public function toggle(Request $request)
    {
        $invite = Invite::where('id', $request->id)->first();
        $invite->used = !boolval($invite->used);
        $invite->save();

        if  ($request->ajax()) {
            return [boolval($invite->used)];
        }

        return redirect()->route('invite')->with('message-success', ['title' => 'Toggled invite', 'message' => 'Successfully toggled invite']);
    }

    public function delete(Request $request)
    {
        Invite::where('id', $request->id)->first()->delete();

        if  ($request->ajax()) {
            return '';
        }

        return redirect()->route('invite')->with('message-success', ['title' => 'Deleted invite', 'message' => 'Successfully deleted invite']);
    }
}
