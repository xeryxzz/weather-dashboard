<?php

namespace App\Http\Controllers;

use App\Category;
use App\Measurement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class ApiController extends Controller
{
    public function pair(Request $request)
    {
        $validate_json = Validator::make(['content' => $request->getContent()], [
            'content' => 'required|json'
        ]);

        if ($validate_json->fails()) {
            return response()->json(['status' => 'Error', 'messages' => $validate_json->errors()], 400);
        }

        $data = json_decode($request->getContent(), true);

        if (!is_array($data)) {
            return response()->json(['status' => 'Error', 'messages' => 'Invalid JSON'], 400);
        }

        $validator_message = Validator::make($data, [
            'measurements.*.name' => 'required|distinct',
            'measurements.*.unit' => 'required'
        ]);


        if ($validator_message->fails()) {
            return response()->json(['status' => 'Error', 'messages' => $validator_message->errors()], 400);
        }

        $measurements = [];
        $categories = $request->sensor->categories;
        $categories_was_empty = $categories->isEmpty();
        $i = 0;
        $total = count($data['measurements']);
        foreach ($data['measurements'] as $measurement) {
            if (!$categories->contains('name', $measurement['name'])) {
                $category = new Category;
                $category->user_id = $request->sensor->user_id;
                $category->sensor_id = $request->sensor->id;
                $category->name = $measurement['name'];
                $category->unit = $measurement['unit'];
                if ($categories_was_empty) {
                    $category->color = $this->HSVtoRGB((360 / $total) * $i, 86, 95);
                }
                $category->save();
                $measurements[] = ['name' => $category->name, 'id' => $category->id];
            }
            else {
                $category = $categories->where('name', $measurement['name'])->first();
                $measurements[] = ['name' => $category->name, 'id' => $category->id];
            }
            $i++;
        }

        return response()->json(['status' => 'Paired', 'messages' => ['measurements' => $measurements]]);
    }

    public function add_measurement(Request $request)
    {
        $validate_json = Validator::make(['content' => $request->getContent()], [
            'content' => 'required|json'
        ]);

        if ($validate_json->fails()) {
            return response()->json(['status' => 'Error', 'messages' => $validate_json->errors()], 400);
        }

        $data = json_decode($request->getContent(), true);

        if (!is_array($data)) {
            return response()->json(['status' => 'Error', 'messages' => 'Invalid JSON'], 400);
        }

        $validator_measurement = Validator::make($data, [
            'measurements.*.id' => ['required', 'distinct', Rule::exists('categories')->where('user_id', $request->sensor->user_id)],
            'measurements.*.value' => 'required|numeric'
        ]);

        if ($validator_measurement->fails()) {
            return response()->json(['status' => 'Error', 'messages' => $validate_json->errors()], 400);
        }

        foreach ($data['measurements'] as $insert_measurement) {
            $measurement = new Measurement;
            $measurement->category_id = $insert_measurement['id'];
            $measurement->value = $insert_measurement['value'];
            $measurement->save();
        }

        return response()->json(['status' => 'Ok', 'message' => []]);
    }


    # From: https://stackoverflow.com/a/3597447
    private function HSVtoRGB($H, $S, $V) {
        $H = (($H + 207) % 360) / 360;

        $S /= 100;
        $V /= 100;

        $H *= 6;
        $I = floor($H);
        $F = $H - $I;
        $M = $V * (1 - $S);
        $N = $V * (1 - $S * $F);
        $K = $V * (1 - $S * (1 - $F));
        switch ($I) {
            case 0:
                list($R,$G,$B) = array($V,$K,$M);
                break;
            case 1:
                list($R,$G,$B) = array($N,$V,$M);
                break;
            case 2:
                list($R,$G,$B) = array($M,$V,$K);
                break;
            case 3:
                list($R,$G,$B) = array($M,$N,$V);
                break;
            case 4:
                list($R,$G,$B) = array($K,$M,$V);
                break;
            case 5:
            case 6: //for when $H=1 is given
                list($R,$G,$B) = array($V,$M,$N);
                break;
        }

        $format = '%02x';
        $result = '';
        foreach ([$R, $G, $B] as $color) {
            $result .= sprintf($format, round($color * 255, 0));
        }

        return $result;
    }
}
