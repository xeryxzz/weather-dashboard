<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function show_dashboard(Request $request, $sensor_id = null, $sensor_name = null, $category_id = null, $category_name = null, $type = 'line') {
        $sensors = $request->sensors;
        $categories = $request->categories;

        $current_sensor = $request->current_sensor;
        $current_category = $request->current_category;

        return view('dashboard')->with(compact('sensors', 'current_sensor', 'categories', 'current_category', 'type'));
    }

    public function get_data(Request $request, $sensor_id = null, $sensor_name = null, $category_id = null, $category_name = null, $type = 'line') {
        $current_sensor = $request->current_sensor;
        $current_category = $request->current_category;

        $date_validator = Validator::make($request->all(), [
            'start' => 'required|date_format:Y-m-d H:i',
            'end' => 'required|date_format:Y-m-d H:i'
        ]);

        if ($date_validator->fails()) {
            if ($request->ajax()) {
                return $date_validator->errors()->toJson();
            }
            return redirect()->route('dashboard', ['sensor_id' => $current_sensor->id, 'sensor_name' => $current_sensor->url_name(), 'category_id' => $current_category->id, 'category_name' => $current_category->url_name()]);
        }

        $measurements = $current_category->measurements->where('created_at', '>=', $request->start)->where('created_at', '<=', $request->end)->sortBy('created_at');

        return json_encode(array_values($measurements->toArray()));
    }
}
