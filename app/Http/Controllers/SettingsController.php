<?php

namespace App\Http\Controllers;

use App\User;
use App\Sensor;
use App\Category;
use App\Measurement;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class SettingsController extends Controller
{
    public function show_settings_form()
    {
        $categories = Auth::user()->categories;
        $sensors = Auth::user()->sensors;

        return view('settings')->with(compact('categories', 'sensors'));
    }

    public function change_email(Request $request)
    {
        $user = Auth::user();

        if ($user->email == $request->email) {
            return redirect('settings')->with('message-warning', ['title' => 'Could not change email address', 'message' => 'Address given is already your address!']);
        }

        $validator = Validator::make($request->all(), User::validate_email()['requirements'], User::validate_email()['messages']);
        if ($validator->fails()) {
            return redirect()->route('settings')->withErrors($validator->errors())->with('title', 'Could not change email address');
        }

        $user->email = $request->email;
        $user->save();

        return redirect()->route('settings')->with('message-success', ['title' => 'Changed email', 'message' => 'Email has been changed']);
    }

    public function change_password(Request $request)
    {
        $user = Auth::user();

        if (!Hash::check($request['old-password'], $user->password)) {
            return redirect('settings')->with('message-danger', ['title' => 'Could not change password', 'message' => 'Invalid current password given']);
        }

        $validator = Validator::make($request->all(), User::validate_password()['requirements'], User::validate_password()['messages']);

        if ($validator->fails()) {
            return redirect()->route('settings')->withErrors($validator->errors())->with('title', 'Could not change password');
        }

        $user->password = Hash::make($request->password);
        $user->save();

        return redirect()->route('settings')->with('message-success', ['title' => 'Changed password', 'message' => 'Password has been changed']);
    }

    public function change_color(Request $request)
    {
        $color = trim($request->color, '#');
        $validator = Validator::make(compact('color'), Category::validate_color()['requirements']);

        if ($validator->fails() && !$request->isJson()) {
            return redirect()->route('settings')->withErrors($validator->errors())->with('title', 'Could not change color');
        }

        $category = Auth::user()->categories->where('id', $request->id)->first();

        if (!is_null($category)) {
            $category->color = $color;
            $category->save();
        }

        if ($request->ajax()) {
            return $validator->errors()->toJson();
        }

        return redirect()->route('settings')->with('message-success', ['title' => 'Changed color', 'message' => 'Color has been changed']);
    }

    public function reset_category(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => ['required', Rule::exists('categories')->where('user_id', Auth::id())]
        ]);

        if ($validator->fails()) {
            return redirect()->route('settings')->withErrors($validator->errors())->with('title', 'Could not reset category');
        }

        $category = Auth::user()->categories->where('id', $request->id)->first();

        if (!is_null($category)) {
            Measurement::where('category_id', $category->id)->delete();
        }

        return redirect()->route('settings')->with('message-success', ['title' => 'Reset category', 'message' => 'Category has been reset']);
    }

    public function delete_category(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => ['required', Rule::exists('categories')->where('user_id', Auth::id())]
        ]);

        if ($validator->fails()) {
            return redirect()->route('settings')->withErrors($validator->errors())->with('title', 'Could not delete category');
        }

        $category = Auth::user()->categories->where('id', $request->id)->first();

        if (!is_null($category)) {
            $sensor = $category->sensor;
            Measurement::where('category_id', $category->id)->delete();
            $category->delete();
            if ($sensor->categories->count() == 0) {
                $sensor->delete();
            }
        }

        return redirect()->route('settings')->with('message-success', ['title' => 'Delete category', 'message' => 'Category has been deleted']);
    }

    public function delete_sensor(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => ['required', Rule::exists('sensors')->where('user_id', Auth::id())]
        ]);

        if ($validator->fails()) {
            return redirect()->route('settings')->withErrors($validator->errors())->with('title', 'Could not delete sensor');
        }

        $sensor = Sensor::where('user_id', Auth::id())->where('id', $request->id)->first();

        if (!is_null($sensor)) {
            $categories = Category::where('sensor_id', $sensor->id);
            foreach ($categories->get() as $category) {
                Measurement::where('category_id', $category->id)->delete();
            }
            $categories->delete();
            $sensor->delete();
        }

        return redirect()->route('settings')->with('message-success', ['title' => 'Delete sensor', 'message' => 'Sensor has been deleted']);
    }
}
