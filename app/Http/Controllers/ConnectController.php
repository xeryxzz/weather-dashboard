<?php

namespace App\Http\Controllers;

use Exception;
use App\Sensor;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ConnectController extends Controller
{
    public function show_connect_form()
    {
        $sensors = Auth::user()->sensors;

        return view('connect')->with('title', 'Connect to sensor')->with(compact('sensors'));
    }

    public function generate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'max:30', Rule::unique('sensors')->where('user_id', Auth::id())]
        ]);

        if ($validator->fails()) {
            return redirect()->route('connect')->withErrors($validator->errors());
        }

        $sensor = new Sensor;
        $sensor->user_id = Auth::id();
        $sensor->name = $request->name;
        $sensor->token = null;
        while (is_null($sensor->token) || mb_strlen($sensor->token) !== 48) {
            try {
                $sensor->token =  substr(str_replace(['+', '/', '='], '', base64_encode(random_bytes(48))), 0, 48);
            }
            catch (Exception $e) {
                $sensor->token = null;
            }
        }
        $sensor->save();

        return redirect()->route('connect')->with('title', 'Connect to sensor')->with('message-success', ['title' => 'Successfully generated key', 'message' => 'You may now connect your program with the supplied key']);
    }
}
