<?php

namespace App\Http\Controllers\Auth;

use App\Mail\PasswordResetMail;
use App\Setting;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\PasswordReset;
use Exception;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class ForgotPasswordController extends Controller
{
    public function show_link_request_form ()
    {
        return view('auth.password')->with('title', 'Forgot password');
    }

    public function send_reset_link_email(Request $request)
    {
        $method = (filter_var($request['username-email'], FILTER_VALIDATE_EMAIL)) ? 'email' : 'username';
        $user = User::where($method, $request['username-email'])->first();
        if (!is_null($user)) {
            $password_reset = new PasswordReset();
            $password_reset->user_id = $user->id;

            $password_reset->token = null;
            while (is_null($password_reset->token) || mb_strlen($password_reset->token) !== 48) {
                try {
                    $password_reset->token = bin2hex(random_bytes(24));
                }
                catch (Exception $e) {
                    $password_reset->token = null;
                }
            }

            $password_reset->save();

            Mail::to($user->email)->send(new PasswordResetMail($password_reset));
        }
        return redirect()->route('login')->with('message-success', ['title' => 'Sent password reset link', 'message' => 'An email with a password reset link has been sent to the associated email address']);
    }

    public function show_reset_form($token)
    {
        $password_reset = PasswordReset::where('token', $token)->first();

        if (!is_null($password_reset) && !$password_reset->used) {
            $expired = (new Carbon()) >= $password_reset->created_at->addSecond(intval(Setting::where('key', 'password_reset.expiry')->first()->value));
            if (!$expired) {
                return view('auth.password')->with('title', 'Reset password')->with(compact('token'));
            }
        }

        return redirect()->route('login')->with('message-danger', ['title' => 'Invalid token', 'message' => 'Password reset has either expired or already been used']);
    }

    public function reset(Request $request, $token)
    {
        $password_reset = PasswordReset::where('token', $token)->first();

        if (!is_null($password_reset) && !$password_reset->used) {
            $expired = (new Carbon()) >= $password_reset->created_at->addSecond(intval(Setting::where('key', 'password_reset.expiry')->first()->value));
            dd($expired);
            if (!$expired) {
                $validator = Validator::make($request->all(), [
                    'password' => 'required|min:8',
                    'confirm-password' => 'required|same:password'
                ], [
                    'required' => 'The :attribute field is required',
                    'password.min' => 'Password is less than :min characters',
                    'confirm-password.same' => 'Passwords don\'t match'
                ]);

                if ($validator->fails()) {
                    return redirect()->route('password_reset', compact('token'))->withErrors($validator);
                }

                $user = $password_reset->user;
                $user->password = Hash::make($request->password);
                $user->save();

                $password_reset->used = true;
                $password_reset->save();

                return redirect()->route('login')->with('message-success', ['title' => 'Password reset', 'message' => 'Password has been successfully reset']);
            }
        }

        return redirect()->route('login')->with('message-danger', ['title' => 'Invalid token', 'message' => 'Password reset has either expired or already been used']);
    }
}
