<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Setting;

class LoginController extends Controller
{
    public function show_login_form()
    {
        $registration_enabled = Setting::where('key', '=', 'registration.enabled')->first()->value === 'true';

        return view('auth.login')->with(compact('registration_enabled'));
    }

    public function authenticate(Request $request)
    {
        $method = (filter_var($request['username-email'], FILTER_VALIDATE_EMAIL)) ? 'email' : 'username';
        if (Auth::attempt([$method => $request['username-email'], 'password' => $request['password']], $request['remember-me'])) {
            return redirect()->route('dashboard');
        }
        return redirect()->route('login')->with('message-danger', ['title' => 'Could not login', 'message' => 'Invalid ' . $method . ' and/or password']);
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('login')->with('message-warning', ['title' => 'Logged out', 'message' => 'You have been logged out']);
    }
}
