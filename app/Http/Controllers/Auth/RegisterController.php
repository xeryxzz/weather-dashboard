<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Setting;
use App\Invite;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    public function show_registration_form($token = '')
    {
        return view('auth.register', compact('token'));
    }

    public function register(Request $request, $token = '')
    {
        $invite = null;
        $invite_valid = false;
        if ($token !== '') {
            $invite = Invite::where('token', $token)->first();
            $invite_valid = !is_null($invite) && !$invite->used;
        }

        $requirements = [
            'username' => 'bail|required|min:3|max:30|unique:users'
        ];

        $messages = [
            'required' => 'The :attribute field is required',
            'username.unique' => 'Username already taken',
            'username.min' => 'Username needs to be at least :min characters',
            'username.max' => 'Username needs to be less than :max characters'
        ];

        $requirements = array_merge($requirements, User::validate_password()['requirements'], User::validate_email()['requirements']);
        $messages = array_merge($messages, User::validate_password()['messages'], User::validate_email()['messages']);

        $validator = Validator::make($request->all(), $requirements, $messages);

        if ($request->ajax()) {
            return $validator->errors()->toJson();
        }

        if ($validator->fails()) {
            return redirect()->route('register', compact('token'))->withErrors($validator)->with('input', ['username' => $request->username, 'email' => $request->email]);
        }

        $user = new User;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->save();

        if ($invite_valid) {
            $invite->used = true;
            $invite->save();
        }

        return redirect()->route('login')->with('message-success', ['title' => 'Registration successful!', 'message' => 'You may now login']);
    }

}
