<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new Role();
        $admin->privilege_level = 20;
        $admin->name = 'Admin';
        $admin->save();

        $user = new Role();
        $user->privilege_level = 5;
        $user->name = 'User';
        $user->save();
    }
}
