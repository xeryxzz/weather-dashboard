<?php

use Illuminate\Database\Seeder;
use App\Setting;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $setting = new Setting();
        $setting->user_id = 1;
        $setting->privilege_needed = 20;
        $setting->key = 'registration.enabled';
        $setting->value = 'true';
        $setting->description = 'Enable/Disable Dashboard registration (Registration through invites still enabled)';
        $setting->save();

        $setting = new Setting();
        $setting->user_id = 1;
        $setting->privilege_needed = 20;
        $setting->key = 'privilege.invite';
        $setting->value = '20';
        $setting->description = 'Privilege needed to create invite';
        $setting->save();

        $setting = new Setting();
        $setting->user_id = 1;
        $setting->privilege_needed = 20;
        $setting->key = 'password_reset.expiry';
        $setting->value = '86400';
        $setting->description = 'Time in seconds before password reset expires';
        $setting->save();
    }
}
