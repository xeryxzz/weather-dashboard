# Weather Dashboard
## Deployment
Configure the application to your environment

    cp .env.example .env
    nano .env

    cp docker-compose.yml.example docker-compose.yml
    nano docker-compose.yml

Install Laravel requirements

    docker run --rm --interactive --tty --volume $PWD:/app composer install

Start containers

    docker-compose up -d

Generate application key

    docker-compose exec php-fpm php artisan key:generate

Run migrations

    docker-compose exec php-fpm php artisan migrate:fresh

Seed database

    docker-compose exec php-fpm php artisan db:seed

Go to the dashboard and login as `admin` with the password `admin` and change password!
