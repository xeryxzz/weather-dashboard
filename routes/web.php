<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Auth\LoginController@show_login_form')->middleware('not_auth')->name('login');
Route::post('/', 'Auth\LoginController@authenticate')->middleware('not_auth');
Route::any('/logout', 'Auth\LoginController@logout')->middleware('auth')->name('logout');

Route::get('/dashboard/{sensor_id?}/{sensor_name?}/{category_id?}/{category_name?}/{type?}', 'DashboardController@show_dashboard')->middleware('auth', 'has_been_setup', 'dashboard_validation')->name('dashboard');
Route::post('/dashboard/{sensor_id?}/{sensor_name?}/{category_id?}/{category_name?}/{type?}', 'DashboardController@get_data')->middleware('auth', 'has_been_setup', 'dashboard_validation')->name('dashboard_data');

Route::get('/register/{token?}', 'Auth\RegisterController@show_registration_form')->middleware('not_auth', 'registration_enabled')->name('register');
Route::post('/register/{token?}', 'Auth\RegisterController@register')->middleware('not_auth', 'registration_enabled');

Route::get('/password/reset', 'Auth\ForgotPasswordController@show_link_request_form')->middleware('not_auth')->name('password_reset_request');
Route::post('/password/reset', 'Auth\ForgotPasswordController@send_reset_link_email')->middleware('not_auth');
Route::get('/password/reset/{token}', 'Auth\ForgotPasswordController@show_reset_form')->middleware('not_auth')->name('password_reset');
Route::post('/password/reset/{token}', 'Auth\ForgotPasswordController@reset')->middleware('not_auth');

Route::get('/connect', 'ConnectController@show_connect_form')->middleware('auth')->name('connect');
Route::post('/connect/generate', 'ConnectController@generate')->middleware('auth')->name('generate');
Route::post('/connect/revoke', 'ConnectController@revoke')->middleware('auth')->name('revoke');

Route::get('/settings', 'SettingsController@show_settings_form')->middleware('auth', 'has_been_setup')->name('settings');
Route::post('/settings/email', 'SettingsController@change_email')->middleware('auth', 'has_been_setup')->name('change_email');
Route::post('/settings/password', 'SettingsController@change_password')->middleware('auth', 'has_been_setup')->name('change_password');
Route::post('/settings/dashboard/color', 'SettingsController@change_color')->middleware('auth', 'has_been_setup')->name('change_color');
Route::post('/settings/dashboard/sensor/delete', 'SettingsController@delete_sensor')->middleware('auth', 'has_been_setup')->name('delete_sensor');
Route::post('/settings/dashboard/category/reset', 'SettingsController@reset_category')->middleware('auth', 'has_been_setup')->name('reset_category');
Route::post('/settings/dashboard/category/delete', 'SettingsController@delete_category')->middleware('auth', 'has_been_setup')->name('delete_category');

Route::get('/invite', 'InviteController@show_invite_form')->middleware('auth', 'allowed_to_invite')->name('invite');
Route::post('/invite/create', 'InviteController@create')->middleware('auth', 'allowed_to_invite')->name('create_invite');
Route::post('/invite/toggle', 'InviteController@toggle')->middleware('auth', 'allowed_to_invite')->name('toggle_invite');
Route::post('/invite/delete', 'InviteController@delete')->middleware('auth', 'allowed_to_invite')->name('delete_invite');
