<html>
    <body>
        <style>h1, p { font-family: Arial, sans-serif}</style>
        <h1>{{ config('app.name', 'Weather Dashboard') }}</h1>
        <p>A password reset request has been issued for your account.</p>
        <p>If you requested this password reset, you may go to this link: <a href="{{ $link }}">{{ $link }}</a></p>
        <p>Otherwise, you can safely ignore this message.</p>
    </body>
</html>

