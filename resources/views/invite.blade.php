@extends('layouts.landing')

@section('title', 'Invites')

@section('content')
    @component('components.alert')@endcomponent
    <div class="card text-body">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>URL</th>
                        <th>Used</th>
                        <th>Remove</th>
                    </tr>
                    </thead>
                    <tbody id="invites-table">
                    @foreach($invites as $invite)
                        <tr>
                            <td>{{ $invite->id }}</td>
                            <td><a href="{{ route('register', ['token' => $invite->token]) }}">{{ route('register', ['token' => $invite->token]) }}</a></td>
                            <td><input type="checkbox" class="dash-checkbox-used" data-invite="{{ $invite->id }}"{{ $invite->used ? ' checked' : '' }}></td>
                            <td><button class="dash-button-remove btn btn-danger" data-invite="{{ $invite->id }}"><i class="fas fa-trash"></i> Remove</button></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <form action="{{ route('create_invite') }}" method="post" id="invite-form">
                @csrf
                <button type="submit" class="btn btn-primary"><i class="fas fa-envelope-open-text"></i> Generate invite</button>
            </form>
        </div>
    </div>
@endsection

@section('after-scripts')
    <script>
        var csrf_token = '{{ csrf_token() }}';

        function add_invite_row(id, url) {
            var row = document.createElement('tr');

            var td_id = document.createElement('td');
            td_id.innerHTML = id;
            row.appendChild(td_id);

            var td_a = document.createElement('td');
            var link = document.createElement('a');
            link.href = url;
            link.innerHTML = url;
            td_a.appendChild(link);
            row.appendChild(td_a);

            var td_input = document.createElement('td');
            var input = document.createElement('input');
            input.type = 'checkbox';
            input.className = 'dash-checkbox-used';
            input.setAttribute('data-invite', id);
            td_input.appendChild(input);
            row.appendChild(td_input);

            var td_remove = document.createElement('td');
            var button = document.createElement('button');
            button.className = 'dash-button-remove btn btn-danger';
            button.setAttribute('data-invite', id);
            button.innerHTML = '<i class="fas fa-trash"></i> Remove';
            td_remove.appendChild(button);
            row.appendChild(td_remove);

            document.getElementById('invites-table').appendChild(row);

            attach_events();
        }

        function attach_events() {
            var checkboxes = $('.dash-checkbox-used');
            var buttons = $('.dash-button-remove');

            checkboxes.off();
            buttons.off();

            checkboxes.click(function () {
                $.post('{{ route('toggle_invite') }}', {_token: csrf_token, id: $(this).data('invite')});
            });

            buttons.click(function () {
                $.post('{{ route('delete_invite') }}', {_token: csrf_token, id: $(this).data('invite')});
                $(this).parent().parent().remove();
            });
        }

        $(document).ready(function () {
            $('#invite-form').submit(function (event) {
               event.preventDefault();
               $.post('{{ route('create_invite') }}', {_token: csrf_token}, function (data) {
                   add_invite_row(data.id, data.url);
               });
            });

            attach_events();
        });
    </script>
@endsection
