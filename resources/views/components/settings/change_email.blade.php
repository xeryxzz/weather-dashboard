<form action="{{ route('change_email') }}" method="post">
    <div class="form-group">
        <label for="email">Email</label>
        <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="{{ \Auth::user()->email }}" required>
    </div>
    @csrf
    <button type="submit" class="btn btn-primary"><i class="far fa-edit"></i> Change email</button>
</form>
