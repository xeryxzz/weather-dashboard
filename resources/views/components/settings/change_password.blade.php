@extends('components.reset_password')

@section('action', 'Change')

@section('old-password')
    <div class="form-group">
        <label for="old-password">Old password</label>
        <input type="password" class="form-control" id="old-password" name="old-password" placeholder="Old password" required>
    </div>
@endsection
