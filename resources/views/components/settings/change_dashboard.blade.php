<div class="table-responsive">
    <table class="table">
        <thead>
            <tr>
                <th>Color</th>
                <th>Name</th>
                <th>Unit</th>
                <th>Reset data</th>
                <th>Delete category</th>
            </tr>
        </thead>
        <tbody>
            {{ $slot }}
        </tbody>
    </table>
</div>
