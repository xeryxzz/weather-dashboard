<form action="{{ $slot }}" method="post">
    @yield('old-password')
    <div class="form-group">
        <label for="password">New password</label>
        <input type="password" class="form-control" id="password" name="password" placeholder="New password" required>
    </div>
    <div class="form-group">
        <label for="confirm-password">Confirm new password</label>
        <input type="password" class="form-control" id="confirm-password" name="confirm-password" placeholder="Confirm new password" required>
    </div>
    @csrf
    <button type="submit" class="btn btn-primary"><i class="fas fa-lock"></i> @yield('action', 'Reset') password</button>
</form>
