@if (session('message-danger'))
    <div class="alert alert-danger mt-3">
        <h4 class="alert-heading">{{ session('message-danger')['title'] }}</h4>
        <hr>
        <p>{{ session('message-danger')['message'] }}</p>
    </div>
@endif
@if (session('message-warning'))
    <div class="alert alert-warning mt-3">
        <h4 class="alert-heading">{{ session('message-warning')['title'] }}</h4>
        <hr>
        <p>{{ session('message-warning')['message'] }}</p>
    </div>
@endif
@if (session('message-success'))
    <div class="alert alert-success mt-3">
        <h4 class="alert-heading">{{ session('message-success')['title'] }}</h4>
        <hr>
        <p>{{ session('message-success')['message'] }}</p>
    </div>
@endif
