@extends('layouts.dashboard_base')

@section('title', $current_category['name'])

@section('content')
    <div class="dash-sidebar-wrapper">
        <div class="dash-sidebar bg-dark">
            <ul class="dash-sidebar-list">
                @foreach ($categories as $category)
                    @if ($category->sensor_id == $current_sensor->id)
                        <li>
                            <a href="{{ route('dashboard', ['sensor_id' => $current_sensor->id, 'sensor_name' => $current_sensor->url_name(), 'category_id' => $category->id, 'category_name' => $category->url_name()]) }}"{!! isset($current_category) && $category->id === $current_category->id ? ' class="active"' : '' !!}>
                                <span class="dash-sidebar-selector" style="background-color:#{{ $category->color }}"></span>
                                <span class="dash-sidebar-link-text">{{ $category->name }}</span>
                            </a>
                        </li>
                    @endif
                @endforeach
            </ul>
        </div>
        <div class="dash-content-wrapper">
            <span class="dash-content-fade"></span>
            <div class="dash-content-heading text-white" style="background-color:#{{ $current_category->color }}">
                <h2 class="dash-content-heading-text p-3 m-0"><button class="dash-sidebar-toggler btn btn-outline-light mr-3"><i class="fas fa-bars"></i></button>{{ $current_category->name }}</h2>
                <hr class="mt-0">
                <p class="dash-content-description mx-3 pt-1 pb-3 my-0">&ndash; {{ $current_category->name }} data from sensor {{ $current_sensor->name }}</p>
            </div>
            @if ($errors->any())
                <div class="alert alert-danger m-3">
                    <h4 class="alert-heading">Could not access page...</h4>
                    <hr>
                    @foreach ($errors->all() as $error)
                        {{ $error }}<br>
                    @endforeach
                </div>
            @endif
            <div class="dash-content p-3">
                <h3 class="mr-2 d-inline-block"><i class="fas fa-chart-line"></i> Graph</h3>
                <a href="{{ route('dashboard', ['sensor_id' => $current_sensor->id, 'sensor_name' => $current_sensor->url_name(), 'category_id' => $current_category->id, 'category_name' => $current_category->url_name(), 'type' => 'line']) }}" id="dash-show-line">Show line</a>
                &ndash; <a href="{{ route('dashboard', ['sensor_id' => $current_sensor->id, 'sensor_name' => $current_sensor->url_name(), 'category_id' => $current_category->id, 'category_name' => $current_category->url_name(), 'type' => 'bar']) }}" id="dash-show-bars">Show bars</a>
                <div class="d-inline-block float-right col-4">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">Time/Date</div>
                        </div>
                        <input type="text" class="form-control text-center" id="time-and-date" name="time-and-date">
                    </div>
                </div>
                <hr>
                <div class="dash-chart-container">
                    <canvas id="dash-chart"></canvas>
                </div>
                <h3 class="mt-5"><i class="fas fa-table"></i> Data</h3>
                <div id="dash-data-container">
                    <hr>
                    <p class="text-muted">No data available...</p>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('mid-scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.min.js" integrity="sha256-XF29CBwU1MWLaGEnsELogU6Y6rcc5nCkhhx89nFMIDQ=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment-with-locales.min.js" integrity="sha256-AdQN98MVZs44Eq2yTwtoKufhnU+uZ7v2kXnD5vqzZVo=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/3.0.3/daterangepicker.min.js" integrity="sha256-cpdGuiwvKhwkQY18xx0CNJBb9AhGOYKsAz6ea7LaB30=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/3.0.3/daterangepicker.min.css" integrity="sha256-DnG3ryf8FsLvOmNjwO9S4+Fpju6QECDbhLbWCtpn7Bc=" crossorigin="anonymous" />
@endsection

@section('after-scripts')
    <script>
        var dash_chart_type = '{{ $type }}';
        var dash_unit = '{{ $current_category->unit }}';
        var dash_color = {
            r: '{{ hexdec(substr($current_category->color, 0, 2)) }}',
            g: '{{ hexdec(substr($current_category->color, 2, 2)) }}',
            b: '{{ hexdec(substr($current_category->color, 4, 2)) }}'
        };
        var dash_data_url = '{{ route('dashboard_data', ['sensor_id' => $current_sensor->id, 'sensor_name' => $current_sensor->url_name(), 'category_id' => $current_category->id, 'category_name' => $current_category->url_name()]) }}';
        var csrf_token = '{{ csrf_token() }}';
    </script>
    <script src="{{ asset('js/graph_page.js') }}"></script>
@endsection
