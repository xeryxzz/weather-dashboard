@extends('layouts.landing')

@section('title', 'Register')

@section('content')
    @if ($errors->any())
        <div class="alert alert-danger mt-3">
            <h4 class="alert-heading">Could not register...</h4>
            <hr>
            @foreach ($errors->all() as $error)
                {{ $error }}<br>
            @endforeach
        </div>
    @endif
    <form action="{{ route('register', ['token' => $token]) }}" method="post" id="registration-form">
        <div class="form-group">
            <label for="username">Username</label>
            <input type="text" class="form-control" id="username" name="username" placeholder="Username" value="{{ session('input')['username'] }}" required>
        </div>
        <div class="form-group">
            <label for="email">Email</label>
            <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="{{ session('input')['email'] }}" required>
        </div>
        <div class="form-group">
            <label for="password">Password</label>
            <input type="password" class="form-control" id="password" name="password" placeholder="Password" required>
        </div>
        <div class="form-group">
            <label for="confirm-password">Confirm password</label>
            <input type="password" class="form-control" id="confirm-password" name="confirm-password" placeholder="Confirm password" required>
        </div>
        @csrf
        <button type="submit" class="btn btn-primary"><i class="fas fa-user-plus"></i> Register</button>
    </form>
    <h5 class="text-center mt-3">
        <a href="{{ route('login') }}" class="text-white"><i class="fas fa-arrow-left"></i> Back to login</a>
    </h5>
@endsection

@section('after-scripts')
    <script>
        var csrf_token = '{{ csrf_token() }}';
        var url = '{{ route('register', compact('token')) }}';

        function validate_username(element) {
            var username = $.trim(element.value);
            var promise = $.Deferred();

            if (username.length < 3) {
                add_warning(element, 'Username is less than 3 characters');
                promise.resolve(false);
            }
            else if (username.length > 30) {
                add_warning(element, 'Username is greater than 30 characters');
                promise.resolve(false);
            }
            else {
                $.post(url, {_token: csrf_token, username: username}, function(data) {
                    result = JSON.parse(data);
                    if (result.username) {
                        result.username.forEach(function(value) {
                            add_warning(element, value);
                            promise.resolve(false);
                        });
                    }
                    else {
                        add_okay(element, 'Nice name!');
                        promise.resolve(true);
                    }
                });
            }
            return promise;
        }
        function validate_email(element) {
            var email = $.trim(element.value);
            var promise = $.Deferred();

            var email_regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if (!email_regex.test(email)) {
                add_warning(element, 'Invalid email');
                promise.resolve(false);
            }
            else {
                $.post(url, {_token: csrf_token, email: email}, function (data) {
                    result = JSON.parse(data);
                    if (result.email) {
                        result.email.forEach(function (value) {
                            add_warning(element, value);
                            promise.resolve(false);
                        });
                    }
                    else {
                        add_okay(element, 'Looks good!');
                        promise.resolve(true);
                    }
                });
            }
            return promise;
        }
        function validate_password(element) {
            var password = element.value;

            if (password.length < 8) {
                add_warning(element, 'Password is less than 8 characters');
                return false;
            }
            else {
                add_okay(element, 'Cool!');
                return true;
            }
        }
        function validate_confirm_password(element) {
            var password = $('#password')[0].value;
            var confirm = element.value;

            if (confirm !== password) {
                add_warning(element, 'Passwords don\'t match');
                return false;
            }
            else {
                add_okay(element, 'Alright!');
                return true;
            }
        }
        function add_warning(element, message) {
            $(element).parent().find('.alert-success').remove();

            var message_exists = false;
            $(element).parent().find('.alert-danger').each(function() {
                if (this.innerHTML === message) {
                    message_exists = true;
                }
            });
            if (!message_exists) {
                $(element).after('<div class="alert alert-danger mt-2">' + message + '</div>');
            }
        }
        function add_okay(element, message) {
            if ($(element).parent().find('.alert-success').length < 1) {
                $(element).parent().find('.alert-danger').remove();
                $(element).after('<div class="alert alert-success mt-2">' + message + '</div>');
            }
        }
        function remove_messages(element) {
            $(element).parent().find('.alert').remove();
        }


        $(document).ready(function() {
            $('#username').keyup(function(){validate_username(this);});
            $('#email').blur(function(){validate_email(this);});
            $('#email').focus(function(){remove_messages(this);});
            $('#password').blur(function(){validate_password(this);});
            $('#password').focus(function(){remove_messages(this)});
            $('#confirm-password').keyup(function(){validate_confirm_password(this);});
            $('#registration-form').submit(function(event) {
                event.preventDefault();
                form = this;

                password_valid = validate_password($('#password')[0]);
                confirm_password_valid = validate_confirm_password($('#confirm-password')[0]);

                validate_username($('#username')[0]).done(function (username_valid) {
                    validate_email($('#email')[0]).done(function (email_valid) {
                        if (username_valid && email_valid && password_valid && confirm_password_valid) {
                            console.log(this);
                            form.submit();
                        }
                        else {
                            alert('You need to fill out all fields correctly!');
                        }
                   });
                });
            });
        });
    </script>
@endsection
