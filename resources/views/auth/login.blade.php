@extends('layouts.landing')

@section('title', 'Login')

@section('content')
    @component('components.alert')@endcomponent
    <form action="{{ route('login') }}" method="post">
        <div class="form-group">
            <label for="username-email">Username/Email</label>
            <input type="text" class="form-control" id="username-email" name="username-email" placeholder="Username/Email" value="{{ session('input')['username-email'] }}" required>
        </div>
        <div class="form-group">
            <label for="password">Password</label>
            <input type="password" class="form-control" id="password" name="password" placeholder="Password">
        </div>
        @csrf
        <button type="submit" class="btn btn-primary"><i class="fas fa-sign-in-alt"></i> Login</button>
        <div class="custom-control custom-checkbox custom-control-inline align-middle m-3">
            <input type="checkbox" class="custom-control-input" id="remember-me" name="remember-me">
            <label class="custom-control-label" for="remember-me">Remember me</label>
        </div>
        <a href="{{ route('password_reset_request') }}" class="float-right align-middle my-3 ml-3 text-white">Forgot password</a>
    </form>
    @if ($registration_enabled)
        <h6 class="text-center mt-3">Not registered?</h6>
        <h5 class="text-center mt-2"><a href="{{ route('register') }}" class="text-white"><i class="fas fa-user-plus"></i> Create account</a></h5>
    @endif
@endsection
