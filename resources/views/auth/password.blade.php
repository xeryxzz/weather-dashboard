@extends('layouts.landing')

@section('title', $title)

@section('content')
    @if ($errors->any())
        <div class="alert alert-danger mt-3">
            <h4 class="alert-heading">Could not reset password...</h4>
            <hr>
            @foreach ($errors->all() as $error)
                {{ $error }}<br>
            @endforeach
        </div>
    @endif
    @component('components.alert')@endcomponent
    @if (isset($token))
        @component('components.reset_password')
            {{ route('password_reset', compact('token')) }}
        @endcomponent
    @else
        <form action="{{ route('password_reset_request') }}" method="post">
            <div class="form-group">
                <label for="username-email">Username/Email</label>
                <input type="text" class="form-control" id="username-email" name="username-email" placeholder="Username/Email" required>
            </div>
            @csrf
            <button type="submit" class="btn btn-primary"><i class="fas fa-envelope"></i> Send link</button>
        </form>
    @endif
@endsection
