@extends('layouts.landing')

@section('title', $title)

@section('content')
    @component('components.alert')@endcomponent
    @if ($errors->any())
        <div class="alert alert-danger mt-3">
            <h4 class="alert-heading">Could not generate key...</h4>
            <hr>
            @foreach ($errors->all() as $error)
                {{ $error }}<br>
            @endforeach
        </div>
    @endif
    @if ($sensors->isEmpty())
        <h5 class="mt-3">Generate a key to get started!</h5>
    @else
        <h5 class="mt-3">Generate a new key</h5>
    @endif
    <form action="{{ route('generate') }}" method="post">
        <div class="input-group">
            <input type="text" class="form-control" placeholder="Sensor name" name="name" id="name">
            <div class="input-group-append">
                <button type="submit" class="btn btn-success">
                    <i class="fas fa-key"></i> Generate key
                </button>
            </div>
        </div>
        @csrf
    </form>
    @if ($sensors->isNotEmpty())
        <h5 class="mt-4">Paste key into the sensor program</h5>
        @foreach($sensors as $sensor)
            <div class="input-group mb-2">
                <div class="input-group-prepend">
                    <span class="input-group-text">Sensor: {{ $sensor->name }}</span>
                </div>
                <input type="password" class="form-control dash-sensor-token" value="{{ $sensor->token }}" placeholder="Key" readonly>
                <div class="input-group-append">
                    <button class="btn btn-light dash-visible-button" type="button" data-placement="top" data-toggle="tooltip" title="View key">
                        <i class="fas fa-eye"></i>
                    </button>
                    <button class="btn btn-light dash-copy-button" type="button" data-placement="top" data-toggle="tooltip" title="Copy to Clipboard">
                        <i class="fas fa-clipboard"></i>
                    </button>
                </div>
            </div>
        @endforeach
    @endif
    <h5 class="text-center mt-4"><a href="{{ route('dashboard') }}" class="text-white">Go to Dashboard <i class="fas fa-arrow-right"></i></a></h5>
    <h6 class="text-center mt-3"><a href="{{ route('logout') }}" class="btn btn-primary"><i class="fas fa-sign-out-alt"></i> Logout</a></h6>
@endsection

@section('after-scripts')
    <script>
        $(document).ready(function() {
            var visible_buttons = $('.dash-visible-button');
            var copy_buttons = $('.dash-copy-button');

            visible_buttons.tooltip();
            copy_buttons.tooltip();

            visible_buttons.click(function() {
                var input = $(this).parent().parent().find('.dash-sensor-token')[0];
                input.type = (input.type === 'text') ? 'password' : 'text';

                var message = $(this).attr('data-original-title');
                $(this).attr('data-original-title', message === 'View key' ? 'Hide key' : 'View key').tooltip('show');

                icon = $(this).find('i');
                icon.toggleClass('fa-eye');
                icon.toggleClass('fa-eye-slash');
            });

            copy_buttons.click(function() {
                var original_message = $(this).attr('data-original-title');

                var textarea = document.createElement("textarea");
                textarea.value = $(this).parent().parent().find('.dash-sensor-token').val();
                document.body.appendChild(textarea);
                textarea.select();

                var message = '';
                try {
                    var successful = document.execCommand('copy');
                    message = successful ? 'Copied!' : 'Could not copy, try with Ctrl+C';
                } catch (err) {
                    message = 'Could not copy, try with Ctrl+C';
                }

                document.body.removeChild(textarea);

                $(this).attr('data-original-title', message).tooltip('show');
                $(this).attr('data-original-title', original_message);
            });
        });
    </script>
@endsection
