@extends('layouts.dashboard_base')

@section('title', 'Profile settings')

@section('content')
    <div class="dash-sidebar-wrapper">
        <div class="dash-content-heading text-white" style="background-color: #2196F3;">
            <h2 class="container pt-3">Profile settings &ndash; {{ \Auth::user()->username }}</h2>
            <hr>
            <p class="dash-content-description container px-3 pt-1 pb-3 my-0">&ndash; Change your profile and settings</p>
        </div>
        <div class="container pt-3">
            @if ($errors->any())
                <div class="alert alert-danger mt-3">
                    <h4 class="alert-heading">{{ session('title') }}</h4>
                    <hr>
                    @foreach ($errors->all() as $error)
                        {{ $error }}<br>
                    @endforeach
                </div>
            @endif
            @component('components.alert')@endcomponent
            <div class="card">
                <div class="card-header text-white bg-primary">Email</div>
                <div class="card-body">
                    @component('components.settings.change_email')@endcomponent
                </div>
            </div>
            <div class="card my-3">
                <div class="card-header text-white bg-primary">Change password</div>
                <div class="card-body">
                    @component('components.settings.change_password'){{ route('change_password') }}@endcomponent
                </div>
            </div>
            <div class="card my-3">
                <div class="card-header text-white bg-primary">Key</div>
                <div class="card-body">
                    <a href="{{ route('connect') }}" class="btn btn-primary"><i class="fas fa-key"></i> Sensor key</a>
                </div>
            </div>
            @foreach ($sensors as $sensor)
                <div class="card my-3">
                    <div class="card-header text-white bg-primary">Sensor &ndash; {{ $sensor->name }}</div>
                    <div class="card-body">
                        @component('components.settings.change_dashboard')
                            @foreach($categories as $category)
                                @if ($category->sensor_id == $sensor->id)
                                    <tr>
                                        <td><input type="color" class="form-control dash-color" data-category="{{ $category->id }}" style="height: 40px; width: 50px" value="#{{ $category->color }}" required></td>
                                        <td>{{ $category->name }}</td>
                                        <td>{{ $category->unit }}</td>
                                        <td><button class="dash-reset-category btn btn-warning" data-category="{{ $category->id }}"><i class="fas fa-redo"></i> Reset</button></td>
                                        <td><button class="dash-delete-category btn btn-danger" data-category="{{ $category->id }}"><i class="fas fa-trash"></i> Delete</button></td>
                                    </tr>
                                @endif
                            @endforeach
                        @endcomponent
                        <button class="dash-delete-sensor btn btn-danger" data-sensor="{{ $sensor->id }}"><i class="fas fa-trash"></i> Delete sensor</button>
                    </div>
                </div>
            @endforeach
            @if (\Auth::user()->role->privilege_level >= intval(\App\Setting::where('key', 'privilege.invite')->first()->value))
                <div class="card my-3">
                    <div class="card-header text-white bg-primary">Invites</div>
                    <div class="card-body">
                        <a href="{{ route('invite') }}" class="btn btn-primary"><i class="fas fa-user-plus"></i> Go to invites</a>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection

@section('after-scripts')
    <script>
        var csrf_token = '{{ csrf_token() }}';

        function submit_form(element, url, id_name, data_name) {
            input_id = document.createElement('input');
            input_id.name = 'id';
            input_id.value = $(element).data(data_name);
            input_id.type = 'text';

            input_csrf = document.createElement('input');
            input_csrf.name = '_token';
            input_csrf.value = csrf_token;
            input_csrf.type = 'text';

            form = document.createElement('form');
            form.id = id_name;
            form.action = url;
            form.method = 'POST';
            form.appendChild(input_id);
            form.appendChild(input_csrf);

            document.body.appendChild(form);

            $('#' + id_name).submit();
        }

        $(document).ready(function () {
            $('.dash-color').change(function () {
                $.post('{{ route('change_color') }}', {_token: csrf_token, id: $(this).data('category'), color: this.value});
            });

            $('.dash-reset-category').click(function () {
                submit_form(this, '{{ route('reset_category') }}', 'reset-category-form', 'category');
            });

            $('.dash-delete-category').click(function () {
                submit_form(this, '{{ route('delete_category') }}', 'delete-category-form', 'category');
            });

            $('.dash-delete-sensor').click(function () {
                submit_form(this, '{{ route('delete_sensor') }}', 'delete-sensor-form', 'sensor');
            });
        });
    </script>
@endsection
