@extends('layouts.base')

@section('body-style', 'background-color: #2196F3')

@section('body')
    <div class="container-fluid h-100">
        <div class="row h-100">
            <div class="col-xs-12 col-sm-10 col-md-8 col-lg-6 m-auto text-white">
                <h1 class="text-center">{{ config('app.name', 'Weather Dashboard') }}</h1>
                <h3>@yield('title')</h3>
                @yield('content')
            </div>
        </div>
    </div>
@endsection
