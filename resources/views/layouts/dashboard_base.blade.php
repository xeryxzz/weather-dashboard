@extends('layouts.base')

@section('body')
    <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark">
        <span class="navbar-brand">{{ config('app.name', 'Weather Dashboard') }}</span>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <span class="mr-auto"></span>
            <ul class="navbar-nav">
                <li class="nav-item{{ \Request::route()->getName() == 'dashboard' ? ' active' : '' }}">
                    <a class="nav-link" href="{{ route('dashboard') }}">
                        <i class="fas fa-chart-line"></i> Dashboard
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbar-dropdown" role="button" data-toggle="dropdown">
                        <i class="fas fa-broadcast-tower"></i> Sensors
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbar-dropdown">
                        @foreach($sensors as $sensor)
                            @if ($sensor->categories->isNotEmpty())
                                <a class="dropdown-item{{ isset($current_sensor) && $sensor->id === $current_sensor->id ? ' active' : '' }}" href="{{ route('dashboard', ['sensor_id' => $sensor->id, 'sensor_name' => $sensor->url_name()]) }}">{{ $sensor->name }}</a>
                            @endif
                        @endforeach
                    </div>
                </li>
                <li class="nav-item{{ \Request::route()->getName() == 'settings' ? ' active' : '' }}">
                    <a class="nav-link" href="{{ route('settings') }}">
                        <i class="fas fa-user"></i> {{ \Auth::user()->username }}
                    </a>
                </li>
                <li class="nav-item{{ \Request::route()->getName() == 'logout' ? ' active' : '' }}">
                    <a class="nav-link" href="{{ route('logout') }}">
                        <i class="fas fa-sign-out-alt"></i> Logout
                    </a>
                </li>
            </ul>
        </div>
    </nav>
    @yield('content')
@endsection
