$(document).ready(function () {
    $(".dash-sidebar-toggler").click(function() {
        if (window.innerWidth < 992) {
            $(".dash-sidebar-wrapper").toggleClass("dash-xs-active");
            $("body").toggleClass("dash-xs-scroll-hidden");
        }
        else {
            $(".dash-sidebar-wrapper").toggleClass("dash-lg-inactive");
        }
    });
    $(".dash-content-fade").click(function() {
        $(".dash-sidebar-wrapper").toggleClass("dash-xs-active");
        $("body").toggleClass("dash-xs-scroll-hidden");
    });
    $(".dash-sidebar-list a").click(function() {
        $(this).addClass("active");
    });
});
