var dash_page = 1;
var dash_items_per_page = 25;
var dash_length = 0;
var dash_pages = 0;
var dash_chart = '';
var dash_values = [];
var dash_dates = [];
var dash_ctx = document.getElementById('dash-chart').getContext('2d');

function show_data(start, end) {
    var start_formatted = start.utc().format('YYYY-MM-DD HH:mm');
    var end_formatted = end.add(1, 'minutes').utc().format('YYYY-MM-DD HH:mm');

    $.post(dash_data_url, {_token: csrf_token, start: start_formatted, end: end_formatted}, function(data) {
        var parsed = JSON.parse(data);

        dash_values = [];
        dash_dates = [];

        parsed.forEach(function (element, index) {
            dash_values[index] = element.value;
            dash_dates[index] = moment.utc(element.date).local().format('L LTS');
        });

        dash_length = Math.min(dash_values.length, dash_dates.length);
        dash_pages = Math.ceil(dash_length / dash_items_per_page);

        build_graph();
        build_data_table();
    });
}

function build_graph() {
    if (dash_chart) {
        dash_chart.destroy();
    }
    dash_chart = new Chart(dash_ctx, {
        type: dash_chart_type,
        data: {
            labels: dash_dates,
            datasets: [{
                label: dash_unit,
                data: dash_values,
                backgroundColor: 'rgba(' + dash_color.r + ',' + dash_color.g + ',' + dash_color.b + ',.4)',
                borderColor: 'rgba(' + dash_color.r + ',' + dash_color.g + ',' + dash_color.b + ',1)',
                borderWidth: 1
            }]
        },
        options: {
            responsive: true,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    },
                }],
                xAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: 'date / time'
                    }
                }]
            }
        }
    });
}

function build_data_table() {
    if (dash_length > 0) {
        var p = $('#dash-data-container');
        var page_items = '';
        for (var i = 1; i <= dash_pages; i++) {
            page_items += '<li class="page-item' + (i === dash_page ? ' active' : '') + '"><button type="button" class="page-link" data-page="' + i + '">' + i + '</button></li>';
        }
        var pagination = '<nav><ul class="pagination justify-content-center"><li class="page-item' + (dash_page > 1 ? '' : ' disabled') + '"><button type="button" class="page-link" data-page="-1">Previous</button></li>' + page_items + '<li class="page-item' + (dash_page < dash_pages ? '' : ' disabled') + '"><button type="button" class="page-link" data-page="+1">Next</button></li></ul></nav>';
        var table = '<div class="table-responsive"><table class="table table-hover"><thead><tr><th scope="col">Value</th><th scope="col">Date</th><th scope="col">Time</th></tr></thead><tbody></tbody></table>' + pagination + '</div>';
        p.empty();
        p.append(table);
        var tb = p.find('tbody');
        for (var i = dash_length - 1 - ((dash_page - 1) * dash_items_per_page); i >= dash_length - ((dash_page - 1) * dash_items_per_page) - dash_items_per_page && i >= 0; i--) {
            var date = dash_dates[i].split(' ');
            tb.append('<tr><td>' + dash_values[i] + ' ' + dash_unit + '</td><td>' + date[0] + '</td><td>' + date[1] + '</td></tr>');
        }

        $('.page-link').off();

        $('.page-link').click(function() {
            var val = $(this).data('page');
            if (val === '+1') {
                dash_page++;
            }
            else if (val === -1) {
                dash_page--;
            }
            else {
                dash_page = parseInt(val);
            }

            build_data_table();
        });
    }
}

$(document).ready(function() {
    moment.locale('sv');
    time_24_hour = !moment.localeData().longDateFormat('LT').toLowerCase().includes('a');
    $('#time-and-date').daterangepicker({
        opens: 'left',
        timePicker: true,
        timePicker24Hour: time_24_hour,
        startDate: moment().startOf('day'),
        endDate: moment().startOf('minute'),
        locale: {
            format: 'L LT'
        }
    }, function(start, end) {
        show_data(start, end);
    });

    show_data(moment().startOf('day'), moment().startOf('minute'));

    $('#dash-show-line').click(function(event) {
        event.preventDefault();
        if (dash_chart_type !== 'line') {
            dash_chart_type = 'line';
            build_graph();
        }
    });

    $('#dash-show-bars').click(function(event) {
        event.preventDefault();
        if (dash_chart_type !== 'bar') {
            dash_chart_type = 'bar';
            build_graph();
        }
    });
});
